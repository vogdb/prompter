function testTarget() {
  return document.getElementById('target')
}

function typeInTarget(value) {
  testTarget().value = value
  happen.once(testTarget(), {type: 'focus'})
}

function pressKeyCode(element, keyCode) {
  happen.once(element, {type: 'keydown', keyCode: keyCode})
  happen.once(element, {type: 'keypress', keyCode: keyCode})
  happen.once(element, {type: 'keyup', keyCode: keyCode})
}

function prompterLen(prompter) {
  var len = 0
  var children = prompter.children
  for (var i = 0; i < children.length; i++) {
    if (children[i].style.display != 'none') {
      len++
    }
  }
  return len
}