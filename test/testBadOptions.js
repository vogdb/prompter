module("incorrect constructor options")
test("no options", 1, function () {
  try {
    new Prompter()
  } catch (err) {
    ok(err.message)
  }
})
test("no target", 1, function () {
  try {
    new Prompter({data: testData()})
  } catch (err) {
    ok(err.message)
  }
})
test("no data", 1, function () {
  try {
    new Prompter({target: testTarget()})
  } catch (err) {
    ok(err.message)
  }
})
