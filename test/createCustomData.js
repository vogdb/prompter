function createCustomData() {
  var data = []
  data.push(createCustomItem('brown', 'potato'))
  data.push(createCustomItem('red', 'tomato'))
  data.push(createCustomItem('pink', 'tomato', 'dfdf'))
  data.push(createCustomItem('green', 'cucumber'))
  return data

  function createCustomItem(color, text, value) {

    var colorElem = document.createElement('div')
    colorElem.style.display = 'inline-block'
    colorElem.style.backgroundColor = color
    colorElem.style.width = '1em'
    colorElem.style.height = '1em'

    var textElem = document.createElement('div')
    textElem.style.display = 'inline-block'
    textElem.innerHTML = text

    var html = document.createElement('div')
    html.appendChild(colorElem)
    html.appendChild(textElem)

    var item = {
      text: text,
      html: html
    }
    if (value) {
      item.value = value
    }
    return item
  }

}
