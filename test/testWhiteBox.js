whiteBox(['potato', 'tomato', 'romato', 'cucumber'])
whiteBox(createCustomData())

function whiteBox(data) {
  var prompter
  var prompterList
  module("white box", {
    setup: function () {
      prompter = new Prompter({target: testTarget(), data: data})
      prompterList = document.getElementsByClassName('prompter')[0]
    },
    tearDown: function () {
      prompterList.parentNode.removeChild(prompterList)
    }
  });
  function filterTest(text, promptListSize) {
    asyncTest("filter", 1, function () {
      typeInTarget(text)
      setTimeout(function () {
        deepEqual(prompterLen(prompterList), promptListSize)
        start()
      }, 1000)
    })
  }

  filterTest('ato', 3)
  filterTest('apple', 0)
  filterTest('cuc', 1)

  asyncTest("select prompt", 2, function () {
    typeInTarget('ato')
    setTimeout(function () {
      var li = prompterList.lastChild
      var promptText = li.getPrompt().text
      happen.once(li, {type: 'click'})
      setTimeout(function () {
        deepEqual(prompterList.style.display, 'none')
        deepEqual(testTarget().value, promptText)
        start()
      }, 500)
    }, 1000)
  })

  asyncTest("enter prompt", 2, function () {
    typeInTarget('cuc')
    setTimeout(function () {
      var li = prompterList.lastChild
      var promptText = li.getPrompt().text
      pressKeyCode(testTarget(), 13)
      setTimeout(function () {
        deepEqual(prompterList.style.display, 'none')
        deepEqual(testTarget().value, promptText)
        start()
      }, 500)
    }, 1000)
  })

  test("dispatch events", 3, function () {

    stop(2)

    testTarget().addEventListener('prompt_select', function (e) {
      //expected value has a weird calculation because data can be an array of strings or objects
      var expectedHtml = data[3].html ? data[3].html : data[3]
      var expectedText = data[3].text ? data[3].text : data[3]
      equal(e.detail.prompt.html, expectedHtml)
      equal(e.detail.prompt.text, expectedText)
      start()
    })
    testTarget().addEventListener('prompt_reset', function () {
      ok(true)
      start()
    })
    typeInTarget('cuc')
    setTimeout(function () {
      happen.once(prompterList.lastChild, {type: 'click'})
      setTimeout(function () {
        typeInTarget('ato')
      }, 500)
    }, 1000)
  })
}