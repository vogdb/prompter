var Prompter = function (opts) {
  this.target
  this.prompts
  this.list
  this._targetChangedInterval

  this.init(opts)
}

Prompter.prototype.init = function (opts) {
  this._initOpts(opts)
  this._initList()
  this._initListeners()
}

Prompter.prototype._initOpts = function (opts) {
  if (!opts) {
    this._error('Set the options value')
  }
  if (opts.target) {
    this.target = opts.target
  } else {
    this._error('Set the target value')
  }
  if (opts.data) {
    this._initPrompts(opts.data)
  } else {
    this._error('Set the data value')
  }
}

Prompter.prototype._initPrompts = function (data) {
  if (Array.isArray(data)) {
    this.prompts = []
    for (var i = 0; i < data.length; i++) {
      if (_isString(data[i])) {
        this.prompts.push({text: data[i], html: data[i], value: data[i]})
      } else {
        this.prompts.push({text: data[i].text, html: data[i].html, value: data[i].value ? data[i].value : data[i].text})
      }
    }
  } else {
    this._error('Prompt data must be an array')
  }
}

Prompter.prototype._initList = function () {
  this.list = new PrompterList(this.target, this.prompts)
}

Prompter.prototype._initListeners = function () {
  var self = this
  this.target.onfocus = function () {
    self.list.show()
    self._targetChangedInterval = setInterval(function () {
      self.list.update()
    }, 500)
  }
  this.target.onblur = function () {
    clearInterval(self._targetChangedInterval)
    setTimeout(function () {
      self.list.hide()
    }, 500)
  }
  this.target.onkeyup = function (e) {
    //'enter' key pressed and single prompt remain
    if (e.keyCode == 13 && self.list.getSize() == 1) {
      self.list.selectFirstAndHide()
    }
  }
}

Prompter.prototype._error = function (msg) {
  throw new Error(msg)
}

window.Prompter = Prompter
