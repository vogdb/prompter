function createCustomEvent(type, data) {
  var event = document.createEvent("Events")
  event.initEvent(type, true, true)
  event.detail = data
  return event
}

function _isString(param) {
  return Object.prototype.toString.call(param).indexOf('String') !== -1
}