var PrompterList = function (target, prompts) {
  this.init(target, prompts)
}

PrompterList.prototype.init = function (target, prompts) {
  this.target = target
  this._createUl()
  this.prompts = prompts
  this.promptsLi = this._wrapInLi(prompts)
  this._isSelected = false
}

PrompterList.prototype._createUl = function () {
  this.ul = document.createElement('ul')
  var self = this
  this.ul.onclick = function (e) {
    self._ulClickHandler(e)
  }
  this.ul.className = 'prompter'
  this.target.parentNode.insertBefore(this.ul, this.target.nextSibling)
  this.hide()
}

PrompterList.prototype._ulClickHandler = function (e) {
  var target = e.target
  while (target !== this.ul) {
    if (target.tagName == 'LI') {
      //target is a li
      this.setValue(target)
      return
    }
    target = target.parentNode
  }
}

PrompterList.prototype._wrapInLi = function (prompts) {
  var result = {}
  var ulFragment = document.createDocumentFragment()
  for (var i = 0; i < prompts.length; i++) {
    var li = this._createLi(prompts[i])
    ulFragment.appendChild(li)
    result[prompts[i].value] = li
  }
  this.ul.appendChild(ulFragment)
  return result
}

PrompterList.prototype._createLi = function (prompt) {
  var li = document.createElement("li")
  if (prompt.value !== undefined) {
    li.value = prompt.value
  }
  var html = prompt.html
  var text = prompt.text
  if (_isString(html)) {
    li.innerHTML = html
  } else {
    li.appendChild(html)
  }
  //helper fields and methods
  li.isShown = true
  li.getPrompt = function () {
    return prompt
  }
  return li
}

PrompterList.prototype.update = function (text) {
  if (!text) {
    text = this.target.value
  }
  text = text.toLowerCase()
  if (this._lastText == text) {
    return
  }
  this._lastText = text
  if (this._isSelected) {
    this.resetSelect()
  }
  for (var i = 0; i < this.prompts.length; i++) {
    var prompt = this.prompts[i]
    var li = this.promptsLi[prompt.value]
    var contains = prompt.text.toLowerCase().indexOf(text) !== -1
    if (contains && !li.isShown) {
      this.showLi(li)
    } else if (!contains && li.isShown) {
      this.hideLi(li)
    }
  }
}

PrompterList.prototype.resetSelect = function () {
  this.show()
  this._isSelected = false
  var event = createCustomEvent('prompt_reset')
  this.target.dispatchEvent(event)
}

PrompterList.prototype.setValue = function (li) {
  var prompt = li.getPrompt()
  this.target.value = prompt.text
  this.update(prompt.text)
  var event = createCustomEvent('prompt_select', {prompt: prompt})
  this.target.dispatchEvent(event)
  this.hideLi(li)
  this.hide()
  //IMPORTANT that we set _isSelected at the end after updating with prompt.text
  this._isSelected = true
}

PrompterList.prototype.selectFirstAndHide = function () {
  var children = this.ul.children
  for (var i = 0; i < children.length; i++) {
    if (children[i].isShown) {
      this.setValue(children[i])
      return
    }
  }
}

PrompterList.prototype.getSize = function () {
  var len = 0
  var children = this.ul.children
  for (var i = 0; i < children.length; i++) {
    if (children[i].isShown) {
      len++
    }
  }
  return len
}

PrompterList.prototype.hide = function () {
  this.ul.style.display = 'none'
}

PrompterList.prototype.show = function () {
  this.ul.style.display = 'block'
}

PrompterList.prototype.hideLi = function (li) {
  li.style.display = 'none'
  li.isShown = false
}

PrompterList.prototype.showLi = function (li) {
  li.style.display = 'list-item'
  li.isShown = true
}
